export { default as Home } from './Home';
export { default as About } from './About';
export { default as Posts } from './Posts';
export { default as Post } from './Post';
export { default as Farms } from './Farms';
export { default as Supplies } from './Supplies';