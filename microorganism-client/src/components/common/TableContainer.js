import React, {Component, Fragment} from 'react';

import RemoteCellEditTable from './RemoteCellEditTable';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import SweetAlert from "sweetalert-react";

class TableContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            columns: props.columns,
            errorMessage: null,
            swal: {}
        };
    }

    handleTableChange = (type, { data, cellEdit: { rowId, dataField, newValue } }) => {
        console.log(type, data);
        setTimeout(() => {
            if (newValue === 'test' && dataField === 'name') {
                this.setState(() => ({
                    data,
                    errorMessage: `Oops, product name shouldn't be "test"`
                }));
            } else {
                const result = data.map((row) => {
                    if (row.id === rowId) {
                        const newRow = { ...row };
                        newRow[dataField] = newValue;
                        return newRow;
                    }
                    return row;
                });
                this.setState(() => ({
                    data: result,
                    errorMessage: null
                }));
            }
        }, 2000);
    };

    render() {
        return (
            <Fragment>
                <RemoteCellEditTable
                    data={ this.props.data }
                    columns={this.props.columns}
                    errorMessage={ this.state.errorMessage }
                    onTableChange={ this.handleTableChange }
                />
            </Fragment>
        );
    }
}

export default TableContainer