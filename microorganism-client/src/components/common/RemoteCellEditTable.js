import React, {Fragment} from "react";
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from 'react-bootstrap-table2-editor';

const RemoteCellEditTable = (props) => {
    const cellEdit = {
        mode: 'click',
        errorMessage: props.errorMessage
    };

    return (
        <Fragment>
            <BootstrapTable
                remote={ { cellEdit: true } }
                keyField="_id"
                data={ props.data }
                columns={ props.columns }
                cellEdit={ cellEditFactory(cellEdit) }
                onTableChange={ props.onTableChange }
                striped
                hover
                condensed
            />
        </Fragment>
    );
};

export default RemoteCellEditTable;