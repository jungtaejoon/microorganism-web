import React, { Fragment } from 'react';
import { Pagination } from 'react-bootstrap';

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap-theme.min.css";

function CustomPagination(props) {
    let { activePage, totalItemsCount, onChange, pageRangeDisplayed, itemsCountPerPage } = props;
    activePage = parseInt(activePage);
    totalItemsCount = parseInt(totalItemsCount);
    pageRangeDisplayed = parseInt(pageRangeDisplayed) || 5;
    itemsCountPerPage = parseInt(itemsCountPerPage) || 10;
    const totalPages = Math.ceil(totalItemsCount / itemsCountPerPage);
    let midFrontPage = Math.ceil(activePage - (pageRangeDisplayed / 2));
    let midBackPage = Math.floor(activePage + (pageRangeDisplayed / 2));
    let frontEllipsis = true;
    let backEllipsis = true;
    if (midFrontPage <= 2) {
        midFrontPage = 2;
        frontEllipsis = false;
        midBackPage = pageRangeDisplayed + 1;
        if (midBackPage >= totalPages - 1) {
            midBackPage = totalPages - 1;
            backEllipsis = false;
        }
    }
    if (midBackPage >= totalPages - 1) {
        midBackPage = totalPages - 1;
        backEllipsis = false;
        midFrontPage = totalPages - pageRangeDisplayed;
        if (midFrontPage <= 2) {
            midFrontPage = 2;
            frontEllipsis = false;
        }
    }
    const pageItems = [];
    for (let i = midFrontPage; i <= midBackPage; i++) {
        pageItems.push(
            <Pagination.Item active={i === activePage} onClick={onChange.bind(null, i)} key={i}>{i}</Pagination.Item>
        )
    }
    let isFirstPage = activePage === 1;
    let isLastPage = activePage === totalPages;
    const pagination = (
        <Fragment>
            {/*<Pagination.First*/}
                {/*disabled={isFirstPage}*/}
                {/*onClick={isFirstPage ? undefined : onChange.bind(null, 1)}*/}
            {/*/>*/}
            <Pagination.Prev
                disabled={isFirstPage}
                onClick={isFirstPage ? undefined : onChange.bind(null, activePage - 1)}
            />
            <Pagination.Item active={1 === activePage} onClick={onChange.bind(null, 1)}>{1}</Pagination.Item>
            {frontEllipsis ? <Pagination.Ellipsis /> : undefined}
            {pageItems}
            {backEllipsis ? <Pagination.Ellipsis /> : undefined}
            <Pagination.Item active={totalPages === activePage} onClick={onChange.bind(null, totalPages)}>{totalPages}</Pagination.Item>
            <Pagination.Next
                disabled={isLastPage}
                onClick={isLastPage ? undefined : onChange.bind(null, activePage + 1)}
            />
            {/*<Pagination.Last*/}
                {/*disabled={isLastPage}*/}
                {/*onClick={isLastPage ? undefined : onChange.bind(null, totalPages)}*/}
            {/*/>*/}
        </Fragment>
    );

    return (
        <Pagination>
            {pagination}
        </Pagination>
    )
}

export default CustomPagination;