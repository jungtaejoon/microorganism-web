import React, {Component, Fragment} from 'react';
import {ButtonToolbar, Dropdown, MenuItem} from "react-bootstrap";
import axios from 'common-function/setDefaultAxios.js';
import DatePicker from 'react-datepicker';
import dateformat from 'dateformat';

import "react-datepicker/dist/react-datepicker.css"

import './css/supplyObject.css';

import SweetAlert from "sweetalert-react";
import handleSwal from "common-function/handleSwal";


const combinedObject = (function () {
    const insertType = [
        'select',
        'input',
        'date'
    ];


    const stateType = {
        applied: {
            name: '요청',
            buttonType: 'danger'
        },
        culturing: {
            name: '배양중',
            buttonType: 'info'
        },
        supplied: {
            name: '공급완료',
            buttonType: 'success'
        }
    };

    const MOType = {
        ingestion: {
            name: '음용',
            buttonType: 'primary'
        },
        smell: {
            name: '악취',
            buttonType: 'warning'
        }
    };

    const supplyObject = {
        _id: {
            name: 'ID',
            accessor: '_id'
        },
        farm: {
            insertType: insertType[0],
            name: '농장명',
            accessor: 'farm.name',
            selectOptions: []
        },
        suppliedDate:  {
            insertType: insertType[2],
            name: '공급일',
            accessor: 'suppliedDate',
            width: {
                maxWidth: 120
            }
        },
        state: {
            insertType: insertType[0],
            name: '공급상태',
            defaultValue: 'applied',
            accessor: 'state',
            refType: stateType,
            width: {
                maxWidth: 95
            },
            selectOptions: [
                { value: 'applied', label: '요청' },
                { value: 'culturing', label: '배양중' },
                { value: 'supplied', label: '공급완료' }
            ]
        },
        MO: {
            insertType: insertType[0],
            name: '미생물종류',
            defaultValue: 'ingestion',
            accessor: 'MO',
            refType: MOType,
            width: {
                maxWidth: 95
            },
            selectOptions: [
                { value: 'ingestion', label: '음용' },
                { value: 'smell', label: '악취' }
            ]
        },
        volume: {
            insertType: insertType[1],
            name: '공급량(톤)',
            defaultValue: '1',
            accessor: 'volume',
            width: {
                maxWidth: 90
            }
        },
        culturingStartDate: {
            insertType: insertType[2],
            name: '배양시작일',
            accessor: 'culturingStartDate',
            width: {
                maxWidth: 120
            }
        },
        price: {
            insertType: insertType[1],
            name: '공급가격',
            defaultValue: '0',
            accessor: 'price',
            width: {
                maxWidth: 90
            }
        },
        note: {
            insertType: insertType[1],
            name: '비고',
            accessor: 'note'
        }
    };
    const { _id, farm, suppliedDate, state, MO, volume, culturingStartDate, price, note} = supplyObject;

    const usingGroup = {
        'list-to-taejoon': { farm, suppliedDate, state, MO, volume, culturingStartDate, price, note },
        'list-to-Admin': { farm, suppliedDate, state, MO, volume, culturingStartDate, price, note },
        'list-to-culturing-worker': { suppliedDate, state, MO, volume, culturingStartDate },
        'insert': { farm, state, MO, volume, price, suppliedDate, note }
    };

    const editableObject = [state, MO, volume, price, culturingStartDate, suppliedDate];

    function selectFunction(a) {
        console.log(a.column);
        const {original} = a;
        return (
            <SelectTemp
                original={original}
                keyName={this.accessor}
                valueName={this.name}
                refType={this.refType}
            />
        )

    }

    function inputFunction({original}) {
        return (
            <InputTemp
                original={original}
                keyName={this.accessor}
            />
        )
    }

    editableObject.map(item => {
        if (item.insertType === insertType[0]) {
            item.cellFunction = selectFunction.bind(item)
        } else if (item.insertType === insertType[1] || item.insertType === insertType[2]) {
            item.cellFunction = inputFunction.bind(item)
        }
        return false;
    });

    return {
        supplyObject,
        usingGroup
    };
})();

class SelectTemp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props,
            swal: {},
            tempOriginal: {}
        };
    }

    render() {
        const {original, keyName, valueName, refType} = this.props;
        return (
            <Fragment>
                <ButtonToolbar>
                    <Dropdown id={`${original._id}-${keyName}-dropdown`} className="fullWidth">
                        <Dropdown.Toggle
                            bsStyle={refType[original[keyName]].buttonType}
                            className="fullWidth"
                        >
                            {refType[original[keyName]].name || valueName}
                        </Dropdown.Toggle>
                        <Dropdown.Menu
                            onSelect={async eValue => {
                                const tempObject = JSON.parse(JSON.stringify(original));
                                tempObject[keyName] = eValue;

                                const {data} = await axios.put(`/api/supplies/${original._id}`, {update: tempObject});
                                if (data.success) {
                                    this.setState({
                                        original: {
                                            ...original,
                                            [keyName]: eValue
                                        }
                                    });
                                    handleSwal.call(this, 'success');
                                } else {
                                    console.log(data.error);
                                    handleSwal.call(this, 'fail', {title: data.error.name, text: data.error.message});
                                }
                            }}
                        >
                            {
                                Object.keys(refType).map(key => <MenuItem eventKey={key} key={key} active={key === original[keyName]}>{refType[key].name}</MenuItem>)
                            }
                        </Dropdown.Menu>
                    </Dropdown>
                </ButtonToolbar>
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />
            </Fragment>


        );
    }
}
class InputTemp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props,
            editing: false,
            swal: {},
            nameInput: {}
        };
    }

    update = async (value) => {
        const {original, keyName, tempOriginal} = this.state;
        if (value === tempOriginal[keyName]) return;
        const {data} = await axios.put(`/api/supplies/${original._id}`, {update: {[keyName]: value}});
        if (data.success) {
            this.setState({
                original: {
                    ...original,
                    [keyName]: value
                },
                editing: false,
                updated: true
            });
            handleSwal.call(this, 'success');
        } else {
            this.setState({
                original: tempOriginal
            });
            console.log(data.error);
            handleSwal.call(this, 'fail', {title: data.error.name, text: data.error.message});
        }
    };

    handleKeyDown = (e) => {
        if (e.key === 'Escape') e.target.blur();
        if (e.key !== 'Enter') return;
        const eValue = e.target.value;
        this.update(eValue);
    };

    handleChange = (e) => {
        const {original} = this.state;
        this.setState({
            original: {
                ...original,
                [e.target.name]: e.target.value
            }
        })
    };

    handleBlur = (e) => {
        console.log(e);
        const {updated, tempOriginal} = this.state;
        if (!updated) {
            this.setState({
                original: tempOriginal,
                updated: true,
                editing: false
            })
        }
    };

    handleDoubleClick = (e) => {
        const {original} = this.state;
        this.setState({
            editing: true,
            tempOriginal: JSON.parse(JSON.stringify(original)),
            updated: false
        });
        setTimeout(()=> this.input ? this.input.focus() : undefined, 0);
    };

    handleDateSelect = (day, e) => {
        if (e.nativeEvent.type === 'click') this.update(day);
    };

    render() {
        let {original, keyName, editing} = this.state;

        return (
            <Fragment>
                {
                    editing ?
                        supplyObject[keyName].insertType === 'date' ?
                            <DatePicker
                                autoFocus
                                selected={new Date(original[keyName])}
                                disabledKeyboardNavigation
                                onSelect={this.handleDateSelect}
                                onBlur={this.handleBlur}
                                onKeyDown={this.handleKeyDown}
                                dateFormat="yyyy-MM-dd"
                            />
                            :
                            <input
                                ref={(input) => this.input = input}
                                name={keyName}
                                style={{width: '100%'}}
                                value={original[keyName]}
                                onKeyDown={this.handleKeyDown}
                                onChange={this.handleChange}
                                onFocus={e => e.target.select()}
                                onBlur={this.handleBlur}
                            />
                        :
                        <div
                            onDoubleClick={this.handleDoubleClick}
                        >{
                            ((supplyObject[keyName].insertType === 'date') && original[keyName] && dateformat(original[keyName], 'yyyy-mm-dd'))
                            ||
                            ((supplyObject[keyName].insertType === 'input') && original[keyName])
                        }</div>
                }
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />
            </Fragment>
        );
    }
}

const { supplyObject, usingGroup } = combinedObject;

export { supplyObject, usingGroup };