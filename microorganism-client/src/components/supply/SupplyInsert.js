import React, {Component, Fragment} from "react";
import axios from 'common-function/setDefaultAxios.js';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal} from "react-bootstrap";
import SweetAlert from 'sweetalert-react';
import Select from 'react-select';

import { supplyObject, usingGroup } from './combinedObject';

import handleChange from '../../common-function/handleChange';
import DatePicker from "react-datepicker/es";

class SupplyInsert extends Component {
    constructor(props) {
        super(props);
        const tempObject = JSON.parse(JSON.stringify(supplyObject));
        Object.keys(tempObject).map(key => tempObject[key] = tempObject[key].defaultValue || '');
        this.state = tempObject;
        this.state.tempObject = JSON.parse(JSON.stringify(tempObject));
        this.state.deleteState = {
            isDeleted: false
        };
        this.state.swal = {};
    }

    componentDidMount () {
        this.setFarmOptions();
    }

    setFarmOptions = async () => {
        if (supplyObject.farm.selectOptions.length === 0) {
            const { data } = await axios.get('/api/farms');
            if (data.success) {
                const farmOptions = this.convertFarms(data.data);
                supplyObject.farm.selectOptions = farmOptions;
            } else {
                this.setState({
                    swal: {
                        show: true,
                        title: data.error.name,
                        text: data.error.message,
                        type: 'error',
                        confirmText: '확인',
                        onConfirm: () => this.setState({swal: {}})
                    }
                });
            }

        }
    };

    insert = () => {
        this.setState({
            swal: {
                show: true,
                title: '공급 등록',
                text: '공급을 등록하시겠습니까',
                type: 'warning',
                confirmText: '등록',
                cancelText: '취소',
                onConfirm: this.onInsertConfirm,
                onCancel: () => this.setState({swal: {}})
            }
        })

    };

    onInsertConfirm = async () => {
        this.setState({suppliedDate: new Date(this.state.suppliedDate || new Date()), desiredDate: new Date(this.state.desiredDate || new Date())});
        let supply = this.state;
        let { data } = await axios.post('/api/supplies', supply);
        if (data.success) {
            this.inserted();
            this.setState({
                swal: {
                    show: true,
                    title: '등록 성공',
                    confirmText: '확인',
                    type: 'success',
                    onConfirm: () => this.setState({swal: {}})
                }
            });

        } else {
            this.setState({
                swal: {
                    show: true,
                    title: data.error.name,
                    text: data.error.message,
                    confirmText: '확인',
                    type: 'error',
                    onConfirm: () => this.setState({swal: {}})
                }
            })
        }
    };

    inserted = () => {
        this.init();
        this.props.inserted();
    };

    init = () => {
        this.setState({
            ...this.state.tempObject,
            deleteState: {
                isDeleted: false
            },
            swal: {}
        });
    };

    handleSelectChange = (selectedOption) => {
        if (!selectedOption) return;
        this.setState({ [selectedOption.key]: selectedOption['value'] });
    };

    convertFarms = (farms) => {
        const temp = [];
        farms.map(farm =>
            temp.push({
                value: farm._id,
                label: farm.name,
                key: 'farm'
            })
        );
        return temp;
    };

    handleDateSelect = (key, value) => this.setState({[key]: value});

    render() {
        return (
            <Fragment>
                <Modal onEnter={this.init} show={this.props.modalShow} onHide={this.props.modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>공급 등록</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form horizontal>
                            {
                                Object.keys(supplyObject).map(key => {
                                    return (
                                        <FormGroup controlId={undefined} key={key}>
                                            <Col componentClass={ControlLabel} sm={2}>
                                                {supplyObject[key]['name']}
                                            </Col>
                                            <Col sm={10}>
                                                {
                                                    supplyObject[key]['insertType'] === 'select' && (
                                                        <Select
                                                            isClearable={true}
                                                            options={supplyObject[key].selectOptions}
                                                            value={supplyObject[key].selectOptions.filter(({value}) => value === this.state[key])}
                                                            onChange={this.handleSelectChange}
                                                        />
                                                    )
                                                }
                                                {
                                                    supplyObject[key]['insertType'] === 'input' && (
                                                        <FormControl
                                                            onChange={handleChange.bind(this)}
                                                            value={this.state[key]}
                                                            name={key}
                                                        />
                                                    )
                                                }
                                                {
                                                    supplyObject[key]['insertType'] === 'date' &&
                                                        <DatePicker
                                                            selected={this.state[key] || new Date()}
                                                            onChange={this.handleDateSelect.bind(null, key)}
                                                            dateFormat="yyyy-MM-dd"
                                                        />

                                                }
                                            </Col>
                                        </FormGroup>
                                    )
                                })
                            }
                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button bsStyle="primary" onClick={this.insert}>등록</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Modal.Body>
                </Modal>
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />
            </Fragment>
        )
    }
}

export default SupplyInsert;