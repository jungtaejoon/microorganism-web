import React, {Component, Fragment} from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import ReactTable from 'react-table';

import axios from 'common-function/setDefaultAxios.js';

import { supplyObject, usingGroup } from './combinedObject';
import handleSwal from 'common-function/handleSwal';
import utoa from 'common-function/utoa';

import './css/SupplyList.css';

import {Button} from "react-bootstrap";
import SweetAlert from "sweetalert-react";
import TableContainer from "../common/TableContainer";



class SupplyList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isInserting: false,
            modal: {
                show: false
            },
            docs: [],
            SupplyInsert: null,
            tableLoading: false,
            swal: {}
        };
        // this.columns = this.setBColumns();
        this.columns = this.setColumns();
        this.defaultPageSize = 10;
    }

    init = () => {
        this.setState({});
        this.setState({
            isInserting: false,
            modal: {
                show: false
            },
            docs: [],
            SupplyInsert: null,
            tableLoading: false,
            swal: {}
        });
        this.temp = {};
    };

    getListFromDb = async (pageSize, page) => {
        if (!page) {
            this.init();
            page = 1;
        }
        const {temp} = this;
        const URL = '/api/supplies?page=' + page;
        if (temp.hasOwnProperty(URL)) {
            this.setState(temp[URL]);
        } else {
            const {data} = await axios(URL);
            if (data.success) {
                this.setState(data.data);
                temp[URL] = data.data;
            } else {
                console.log(data.error);
            }

        }
    };

    setColumns = () => {
        const columns = [];
        Object.keys(usingGroup['list-to-taejoon']).map(key => {
            const obj = {
                Header: supplyObject[key]['name'],
                accessor: supplyObject[key]['accessor'],
                Cell: supplyObject[key].cellFunction,
                sortable: true,
                filterable: true
            };
            if (typeof supplyObject[key].width === 'object') {
                Object.keys(supplyObject[key].width).map(childKey => obj[childKey] = supplyObject[key].width[childKey]);

            }
            columns.push(obj);
            return false;
        });
        columns.push({
            Header: '삭제',
            accessor: "delete",
            maxWidth: 80,
            Cell: ({original}) => (
                <Button
                    bsStyle="danger"
                    onClick={() => {
                        handleSwal.call(this, 'confirm', {onConfirm: this.handleOnDeleteConfirm.bind(this, original)});
                    }}
                >삭제</Button>
            )
        });
        return columns;
    };

    // setBColumns = () => {
    //     const columns = [];
    //     Object.keys(usingGroup['list-to-taejoon']).map(key => {
    //         const obj = {
    //             text: supplyObject[key]['name'],
    //             dataField: supplyObject[key]['accessor'],
    //
    //         };
    //         if (supplyObject[key].selectOptions) {
    //             obj.editor = {
    //                 type: Type.SELECT,
    //                 options: supplyObject[key].selectOptions
    //             };
    //         }
    //         console.log(obj);
    //
    //         columns.push(obj);
    //         return false;
    //     });
    //     return columns;
    // };

    // setFarmOptions = async () => {
    //     if (supplyObject.farm.selectOptions.length === 0) {
    //         const { data } = await axios.get('/api/farms');
    //         if (data.success) {
    //             const farmOptions = this.convertFarms(data.data);
    //             supplyObject.farm.selectOptions = farmOptions;
    //         } else {
    //             this.setState({
    //                 swal: {
    //                     show: true,
    //                     title: data.error.name,
    //                     text: data.error.message,
    //                     type: 'error',
    //                     confirmText: '확인',
    //                     onConfirm: () => this.setState({swal: {}})
    //                 }
    //             });
    //         }
    //
    //     }
    //     const columns = this.setColumns();
    //     this.setState({
    //         columns
    //     });
    // };
    // convertFarms = (farms) => {
    //     const temp = [];
    //     farms.map(farm =>
    //         temp.push({
    //             value: farm._id,
    //             label: farm.name
    //         })
    //     );
    //     return temp;
    // };

    inserted = () => {
        this.setState({isInserting: false});
        this.getListFromDb();
    };

    inserting = async () => {
        if (!this.state.SupplyInsert) {
            import('./SupplyInsert').then(({default: SupplyInsert}) => {
                this.setState({
                    SupplyInsert
                });
            });
        }

        this.setState({isInserting: true});
    };


    handleModalClose = () => {
        this.setState({isInserting: false});
    };

    handleOnDeleteConfirm = async (original) => {
        const temp = JSON.parse(JSON.stringify(original));
        temp.deleteState.isDeleted = true;

        const {data} = await axios.put('/api/supplies/' + original._id, {update: temp});
        if (data.success) {
            this.getListFromDb();
            handleSwal.call(this, 'success');
        } else {
            handleSwal.call(this, 'fail', {text: data.error});
        }
    };

    fetchData = async ({page, pageSize, sorted, filtered}, instance) => {
        page++;
        let limit = pageSize;
        let sort = [], filter = [];
        sorted.map(obj => sort.push({[obj.id]: obj.desc ? 'desc' : 'asc'}));
        filtered.map(obj => filter.push({[obj.id]: obj.value}));

        let aSort;
        if (sort.length > 0) aSort = utoa(JSON.stringify(sort));
        let aFilter;
        if (filter.length > 0) aFilter = utoa(JSON.stringify(filter));

        let pageString = `?page=${page}&limit=${limit}`;
        let sortString = `&sort=${aSort}`;
        let filterString = `&filter=${aFilter}`;

        let qString = pageString + (aSort ? sortString : '') + (aFilter ? filterString : '');

        const { data } = await axios.get(`/api/supplies${qString}`);
        // this.setState({tableLoading: true});
        console.log(data)
        this.setState({docs: data.data});
    };

    render() {
        const {SupplyInsert, docs, tableLoading} = this.state;

        return (
            <Fragment>

            <div className="container-fluid" id="tourpackages-carousel">

                <div className="row fix-height">
                    <div className="col-lg-12">
                        <h1>공급내역
                            <button className="btn icon-btn btn-primary pull-right" onClick={this.inserting}>
                                <span className="glyphicon btn-glyphicon glyphicon-plus img-circle"/> 공급 추가
                            </button>
                        </h1>
                    </div>
                    <ReactTable
                        data={docs}
                        columns={this.columns}
                        defaultPageSize={this.defaultPageSize}
                        className="-striped -highlight"
                        getTdProps={() => {
                            return ({
                                style: {
                                    overflow: 'visible',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    textAlign: 'center',
                                }
                            })
                        }}
                        loading={tableLoading}
                        onFetchData={this.fetchData}
                    />
                    {
                        SupplyInsert && <SupplyInsert
                            modalShow={this.state.isInserting}
                            modalClose={this.handleModalClose}
                            inserted={this.inserted}
                        />
                    }
                </div>
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />


            </div>
            </Fragment>
        )
    }

}

export default SupplyList;