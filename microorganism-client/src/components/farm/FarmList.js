import React, { Component } from 'react';

import Farm from './Farm';
import FarmInsert from './FarmInsert';

import axios from 'common-function/setDefaultAxios.js';

import './css/FarmList.css';

import CustomPagination from '../common/CustomPagination';

import 'react-table/react-table.css'

class FarmList extends Component {
    state = {
        isInserting: false,
        modal: {
            show: false
        },
        temp: {},
        docs: []
    };
    componentDidMount() {
        this.getListFromDb();
    }

    getListFromDb = async (page) => {
        if (!page) {
            this.setState({temp: {}});
            page = 1;
        }
        const { temp } = this.state;
        const URL = '/api/farms?page=' + page;
        if (temp.hasOwnProperty(URL)) {
            this.setState(temp[URL]);
        } else {
            const {data} = await axios(URL);
            if (data.success) {
                this.setState(data.data);
                temp[URL] = data.data
            } else {
                console.log(data.error);
            }

        }
    };
    inserted = () => {
        this.setState({isInserting: false});
        this.getListFromDb();
    };

    inserting = () => {
        this.setState({isInserting: true});
    };

    handlePageChange = (page) => {
        this.getListFromDb(page);
    };

    handleModalClose = () => {
        this.setState({ isInserting: false });
    };

    render() {
        const { docs } = this.state;
        return (
            <div>
                <div className="container" id="tourpackages-carousel">
                    <div className="row fix-height">
                        <div className="col-lg-12">
                            <h1>농장 리스트
                                <button className="btn icon-btn btn-primary pull-right" onClick={this.inserting}>
                                    <span className="glyphicon btn-glyphicon glyphicon-plus img-circle" /> 농장 추가
                                </button>
                            </h1>
                        </div>

                        {docs.map(farm => {
                            return (
                                <Farm
                                    updated={this.updated}
                                    deleted={this.getListFromDb}
                                    attr={farm}
                                    key={farm._id}
                                />
                            )
                        })}

                    </div>
                    <div className='text-center'>
                        <CustomPagination
                            activePage={this.state.page}
                            itemsCountPerPage={this.state.limit}
                            totalItemsCount={this.state.total}
                            pageRangeDisplayed={7}
                            onChange={this.handlePageChange}
                        />
                    </div>
                    <FarmInsert
                        modalShow={this.state.isInserting}
                        modalClose={this.handleModalClose}
                        inserted={this.inserted}
                    />
                </div>
            </div>
        )
    }
}

export default FarmList;