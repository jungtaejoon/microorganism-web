import React, {Component, Fragment} from "react";
import axios from 'common-function/setDefaultAxios.js';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal} from "react-bootstrap";
import SweetAlert from 'sweetalert-react';

import handleChange from '../../common-function/handleChange';

class FarmInsert extends Component {
    state = {
        name: '',
        tel: '',
        owner: '',
        address: '',
        location: '',
        deleteState: {
            isDeleted: false
        },
        swal: {}
    };

    insert = () => {
        this.setState({
            swal: {
                show: true,
                title: '농장 등록',
                text: '농장을 등록하시겠습니까',
                type: 'warning',
                confirmText: '등록',
                cancelText: '취소',
                onConfirm: async () => {
                    let farm = this.state;
                    let { data } = await axios.post('/api/farms', farm);
                    if (data.success) {
                        this.setState({
                            swal: {
                                show: true,
                                title: '등록 성공',
                                confirmText: '확인',
                                type: 'success',
                                onConfirm: () => {
                                    this.setState({
                                        name: '',
                                        tel: '',
                                        owner: '',
                                        address: '',
                                        location: '',
                                        deleteState: {
                                            isDeleted: false
                                        },
                                        swal: {}
                                    });
                                    this.props.inserted();
                                }
                            }
                        });

                    } else {
                        this.setState({
                            swal: {
                                show: true,
                                title: '등록 실패',
                                text: JSON.stringify(data.error),
                                confirmText: '확인',
                                type: 'error',
                                onConfirm: () => this.setState({swal: {}})
                            }
                        })
                    }
                },
                onCancel: () => this.setState({swal: {}})
            }
        })

    };



    render() {
        const keyNameValue = {
            name: '농장명',
            tel: '전화번호',
            owner: '농장주',
            address: '주소',
            location: '지역'
        };

        return (
            <Fragment>
                <Modal show={this.props.modalShow} onHide={this.props.modalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>농장 등록</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form horizontal>
                            {
                                Object.keys(keyNameValue).map((key, index) => {
                                    return (
                                        <FormGroup controlId={key} key={index}>
                                            <Col componentClass={ControlLabel} sm={2}>
                                                {keyNameValue[key]}
                                            </Col>
                                            <Col sm={10}>
                                                <FormControl
                                                    placeholder={keyNameValue[key]}
                                                    onChange={handleChange.bind(this)}
                                                    value={this.state[key]}
                                                    name={key}
                                                />
                                            </Col>
                                        </FormGroup>
                                    )
                                })
                            }

                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button bsStyle="primary" onClick={this.insert}>등록</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Modal.Body>
                </Modal>
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />
            </Fragment>
        )
    }
}

export default FarmInsert;