import React, {Component, Fragment} from "react";
import axios from 'common-function/setDefaultAxios.js';
import SweetAlert from 'sweetalert-react';
import classnames from 'classnames';
import dateformat from 'dateformat';


import 'sweetalert/dist/sweetalert.css';

import './css/Farm.css';

import handleChange from '../../common-function/handleChange';

class Farm extends Component {

    constructor(props) {
        super(props);
        this.state = props.attr;
        this.state.editableData = {
            name: '농장명',
            owner: '농장주',
            tel: '연락처',
            address: '주소',
            location: '지역'
        };
        this.state.isModifying = false;
        this.state.swal = {};
    }

    delete = () => {
        this.setState({
            swal: {
                show: true,
                title: '정말로?',
                confirmText: '삭제',
                cancelText: '취소',
                type: 'warning',
                onConfirm: this.onDeleteConfirm,
                onCancel: () => this.setState({swal: {}})
            }
        });

    };

    onDeleteConfirm = async () => {
        this.setState({deleteState: {...this.state.deleteState, isDeleted: true}});
        const {data} = await axios.put('/api/farms/' + this.state._id, {update: this.state});
        if (!data.success) {
            this.setState({
                swal: {
                    show: true,
                    title: '삭제 실패',
                    text: JSON.stringify(data.error),
                    confirmText: '확인',
                    type: 'error',
                    onConfirm: () => this.setState({swal: {}})
                },
                deleteState: {
                    ...this.state.deleteState,
                    isDeleted: false
                }
            });
        } else {
            this.setState({
                swal: {
                    show: true,
                    title: '삭제 성공',
                    confirmText: '확인',
                    type: 'success',
                    onConfirm: () => {
                        this.setState({swal: {}});
                        this.props.deleted();
                    }
                }
            });

        }
    };

    update = () => {
        let isDifferent = false;
        Object.keys(this.state.editableData).map(key => this.state[key] !== this.state.temp[key] && (isDifferent = true));
        if (isDifferent) {
            this.setState({
                swal: {
                    show: true,
                    title: '수정하시겠습니까?',
                    confirmText: '수정',
                    cancelText: '취소',
                    type: 'warning',
                    onConfirm: this.onUpdateConfirm,
                    onCancel: () => this.setState({swal: {}})
                }
            });
        } else {
            this.setState({
                swal: {
                    show: true,
                    title: '변경사항이 없습니다.',
                    confirmText: '확인',
                    type: 'warning',
                    onConfirm: () => this.setState({swal: {}}),
                }
            });
        }


    };

    onUpdateConfirm = async () => {
        const {data} = await axios.put('/api/farms/' + this.state._id, {update: this.state});
        if (!data.success) {
            this.setState({
                swal: {
                    show: true,
                    title: '수정 실패',
                    text: JSON.stringify(data.error),
                    confirmText: '확인',
                    type: 'error',
                    onConfirm: () => this.setState({swal: {}})
                }
            });
        } else {
            this.setState({
                swal: {
                    show: true,
                    title: '수정 성공',
                    confirmText: '확인',
                    type: 'success',
                    onConfirm: () => this.setState({swal: {}})
                }
            });
            if (this.state.isModifying) this.setState({isModifying: false});
        }
    };

    modify = () => {
        this.setState({temp: JSON.parse(JSON.stringify(this.state))});
        this.setState({isModifying: true});
    };


    cancel = () => {
        this.setState(this.state.temp);
    };

    render() {
        const inputH4Class = classnames({
            'transparent-input-h4': true,
            'transparent-input-h4-disabled': !this.state.isModifying
        });

        const inputPCLass = classnames({
            'transparent-input-p': true,
            'transparent-input-p-disabled': !this.state.isModifying
        });

        return (
            <Fragment>
                <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div className="thumbnail">
                        <div className="caption">
                            <div className='well well-add-card'>
                                <span className="glyphicon glyphicon-home" />
                                <span style={{'marginLeft': '13px'}}>
                                    <input
                                        style={{'width': '195px'}}
                                        className={inputH4Class}
                                        name='name'
                                        disabled={!this.state.isModifying}
                                        onChange={handleChange.bind(this)}
                                        value={this.state.name}
                                    />
                                </span>
                            </div>
                            <div style={{padding: '0 16px 16px'}}>
                                {
                                    Object.keys(this.state.editableData).map(key => {
                                        if (key !== 'name') {
                                            return (
                                                <p key={key}>{this.state.editableData[key]}: <input
                                                    className={inputPCLass}
                                                    name={key}
                                                    disabled={!this.state.isModifying}
                                                    onChange={handleChange.bind(this)}
                                                    value={this.state[key]}
                                                /></p>
                                            );
                                        }
                                        return false;
                                    })
                                }
                                <p className="text-muted">{dateformat(new Date(this.state.createdAt), 'yyyy-mm-dd')}</p>
                                {
                                    this.state.isModifying ?
                                        <Fragment>
                                            <button
                                                type="button"
                                                className="btn btn-primary btn-xs btn-update btn-add-card"
                                                onClick={this.update}
                                                style={{'marginRight': '8px'}}
                                            >
                                                확인
                                            </button>
                                            <button
                                                type="button"
                                                className="btn btn-primary btn-xs btn-update btn-add-card"
                                                onClick={this.cancel}
                                            >
                                                취소
                                            </button>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <button
                                                className="btn btn-primary btn-xs btn-update btn-add-card"
                                                onClick={this.modify}
                                            >
                                                수정
                                            </button>

                                        </Fragment>

                                }
                                <span
                                    className='glyphicon glyphicon-trash pull-right text-primary'
                                    onClick={this.delete}
                                    style={{cursor:'pointer'}}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <SweetAlert
                    show={this.state.swal.show}
                    title={this.state.swal.title || '알림'}
                    text={this.state.swal.text}
                    type={this.state.swal.type}
                    confirmButtonText={this.state.swal.confirmText}
                    showConfirmButton={this.state.swal.showConfirmButton}
                    cancelButtonText={this.state.swal.cancelText}
                    showCancelButton={!!this.state.swal.cancelText}
                    onConfirm={this.state.swal.onConfirm}
                    onCancel={this.state.swal.onCancel}
                    onClose={() => this.setState({swal: {}})}
                    onEscapeKey={() => this.setState({swal: {}})}
                    onOutsideClick={() => this.setState({swal: {}})}
                />
            </Fragment>
        )
    }
}

export default Farm;