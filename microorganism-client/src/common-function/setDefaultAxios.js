import axios from 'axios';

export default axios.create({
    // baseURL: `https://us-central1-microorganism-web.cloudfunctions.net`
    baseURL: `http://localhost:3001`
});