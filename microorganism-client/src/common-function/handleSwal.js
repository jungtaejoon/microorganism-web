function handleSwal(string, options) {
    const swalTemplate = {
        success: {
            title: '성공',
            type: 'success'
        },
        fail: {
            title: '실패',
            type: 'error',
            text: options ? options.text : undefined
        },
        confirm: {
            title: '정말로 실행하시겠습니까?',
            type: 'warning',
            text: options ? options.text : undefined,
            cancelText: '취소',
            showConfirmButton: true,
            showCancelButton: true
        }
    };
    options = {
        show: true,
        title: '알림',
        type: 'info',
        onConfirm: () => this.setState({swal: {}}),
        onCancel: () => this.setState({swal: {}}),
        ...swalTemplate[string],
        ...options
    };

    this.setState({swal: options})
}
export default handleSwal;