export default function (baseObject, targetObject) {
    if (typeof baseObject !== 'object' || typeof targetObject !== 'object') throw 'should be a object';
    const tempObj = {
        mainData: {},
        extraData: {}
    };

    Object.keys(targetObject).map(key => {
        const varName = baseObject.hasOwnProperty(key) ? 'mainData' : 'extraData';
        if (typeof targetObject[key] === 'object') tempObj[varName][key] = JSON.parse(JSON.stringify(targetObject[key]));
        else tempObj[varName][key] = targetObject[key];
    });
    const { mainData, extraData } = tempObj;
    return {
        mainData,
        extraData
    };
}