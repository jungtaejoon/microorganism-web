import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import {About, Farms, Home, Posts, Supplies} from "../pages";
import Menu from "../components/Menu";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';

class App extends Component {
    render() {
        return (
            <div style={{padding: '50px 0'}}>
                <Menu />
                <div>
                    <Route exact path="/" component={Home}/>
                    <Switch>
                        <Route path="/about/:name" component={About}/>
                        <Route path="/about" component={About}/>
                    </Switch>
                    <Switch>
                        <Route path="/farms/:id" component={Farms}/>
                        <Route path="/farms" component={Farms}/>
                    </Switch>
                    <Switch>
                        <Route path="/supplies/:id" component={Supplies}/>
                        <Route path="/supplies" component={Supplies}/>
                    </Switch>
                    <Route path="/posts" component={Posts}/>
                </div>
            </div>

        );
    }
}

export default App;