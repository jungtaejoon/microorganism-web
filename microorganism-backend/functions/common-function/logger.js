module.exports = function logger (data, specialLog) {
    const LINE_BREAKER = '-----';
    specialLog = specialLog || 'START';
    const LOGGER_HEAD = LINE_BREAKER + ' ' + specialLog + ' ' + LINE_BREAKER;
    const LOGGER_TAIL = LINE_BREAKER + ' END ' + LINE_BREAKER;
    console.log(LOGGER_HEAD);
    console.log(data);
    console.log(LOGGER_TAIL);
};