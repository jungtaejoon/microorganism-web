const atob = require('atob');

function atou(str) {
    if (typeof str !== 'string') return;
    return decodeURIComponent(escape(atob(str)));
}

module.exports = atou;