// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = new Schema(
    {
        isDeleted: Boolean,

    },
    { timestamps: true }
);
