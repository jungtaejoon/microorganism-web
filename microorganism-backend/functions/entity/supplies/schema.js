// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');



const deleteState = require('../common/deleteSchema');

// this will be our data base's data structure
const SupplySchema = new Schema(
    {
        suppliedDate: Date,
        state: String,
        MO: String,
        volume: Number,
        culturingStartDate: Date,
        price: Number,
        farm: { type: mongoose.Schema.Types.ObjectId, ref: 'Farm' },
        note: String,
        deleteState,
        createdAt: Date,
        updatedAt: Date
    }
);

SupplySchema.plugin(mongoosePaginate);
SupplySchema.plugin(mongooseAggregatePaginate);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Supply", SupplySchema);