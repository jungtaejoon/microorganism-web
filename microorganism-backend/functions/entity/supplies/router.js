const router = require('express').Router();
const Data = require('./schema');
const base = require('./base');
const logger = require('../../common-function/logger');
const atou = require('../../common-function/customAtob');

const PATH = base.path;
const PATH_AND_ID = base.path + '/:id';

router.get(PATH, (req, res) => {
    let {page, limit, sort: sortArray, filter: filterArray} = req.query;
    page = parseInt(page) || 1;
    limit = parseInt(limit) || 10;
    sortArray = sortArray ? JSON.parse(atou(sortArray)) : [{createdAt: 'asc'}];
    filterArray = filterArray ? JSON.parse(atou(filterArray)) : undefined;

    const sortBy = sortArray.reduce(function(result, item) {
        const key = Object.keys(item)[0]; //first property: a, b, c
        result[key] = item[key];
        return result;
    }, {});

    let filter = {};
    if (filterArray) {
        filter = filterArray.reduce(function(result, item) {
            const key = Object.keys(item)[0]; //first property: a, b, c
            result[key] = {$regex: new RegExp(item[key], 'i')};
            return result;
        }, {});
    }

    const options = {
        page,
        limit,
        sortBy,
    };
    const aggregate = Data.aggregate([
        {
            '$match': { 'deleteState.isDeleted': false }
        },

        {
            '$lookup': {
                from: "farms",
                localField: "farm",
                foreignField: "_id",
                as: "farm"
            }
        },
        {
            "$addFields": {
                farm: {
                    "$arrayElemAt": [ "$farm", 0 ]
                },

            }
        }
    ]);
    aggregate.match(filter);
    Data.aggregatePaginate(aggregate, options, function(err, results, pageCount, count) {
        if (err) {
            console.log(err, results);
            return res.json({ success: false, error: err });
        }
        // data = data.filter(item => item.deleteState && !item.deleteState.isDeleted);
        // logger(data, 'THE LIST');
        return res.json({ success: true, data: results, options, filter });

    })
    // Data.paginate(filter, options, (err, data) => {
    //     if (err) {
    //         console.log(err, data);
    //         return res.json({ success: false, error: err });
    //     }
    //     // data = data.filter(item => item.deleteState && !item.deleteState.isDeleted);
    //     // logger(data, 'THE LIST');
    //     return res.json({ success: true, data: data, options, filter });
    // });
});

router.put(PATH_AND_ID, (req, res) => {
    const { update } = req.body;
    update.updatedAt = Date.now();
    const _id = req.params.id;
    // return res.json({ success: false, error: update });
    Data.findOneAndUpdate({_id}, update, (error, item) => {
        if (error) return res.json({ success: false, error });
        logger(update, 'UPDATED ITEM');
        return res.json({ success: true, update });
    });
});

router.post(PATH, (req, res) => {
    let data = new Data();
    Object.assign(data, req.body);
    data.createdAt = Date.now();
    data.updatedAt = Date.now();
    data.save((error, item) => {
        if (error) return res.json({ success: false, error });
        logger(item, 'SAVED ITEM');
        return res.json({ success: true, item});
    });
});

module.exports = router;