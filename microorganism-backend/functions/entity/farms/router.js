const router = require('express').Router();
const Data = require('./schema');
const base = require('./base');
const logger = require('../../common-function/logger');

const PATH = base.path;
const PATH_AND_ID = base.path + '/:id';

router.get(PATH, (req, res) => {
    if (!req.query.page) {
        Data.find({ 'deleteState.isDeleted': false }, (err, data) => {
            if (err) return res.json({ success: false, error: err });
            logger(data, 'THE LIST');
            return res.json({ success: true, data: data });
        });
    } else {
        let page = req.query.page;
        Data.paginate({ 'deleteState.isDeleted': false }, {page, limit: 9, sort: {createdAt: 'asc'}}, (err, data) => {
            if (err) return res.json({ success: false, error: err });
            // data = data.filter(item => item.deleteState && !item.deleteState.isDeleted);
            logger(data, 'THE LIST');
            return res.json({ success: true, data: data });
        });
    }

});

router.put(PATH_AND_ID, (req, res) => {
    const { update } = req.body;
    update.updatedAt = Date.now();
    const _id = req.params.id;
    // return res.json({ success: false, error: update });
    Data.findOneAndUpdate({_id}, update, (error, item) => {
        if (error) return res.json({ success: false, error });
        logger(item, 'UPDATED ITEM');
        return res.json({ success: true, item });
    });
});

router.post(PATH, (req, res) => {
    let data = new Data();
    Object.assign(data, req.body);
    data.createdAt = Date.now();
    data.updatedAt = Date.now();
    data.save((error, item) => {
        if (error) return res.json({ success: false, error });
        logger(item, 'SAVED ITEM');
        return res.json({ success: true, item});
    });
});

module.exports = router;