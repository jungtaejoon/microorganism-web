// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const deleteState = require('../common/deleteSchema');

// this will be our data base's data structure
const FarmSchema = new Schema(
    {
        name: String,
        tel: String,
        owner: String,
        location: String,
        address: String,
        deleteState,
        createdAt: Date,
        updatedAt: Date
    }
);

FarmSchema.plugin(mongoosePaginate);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Farm", FarmSchema);