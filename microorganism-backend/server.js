const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const farmRouter = require('./functions/entity/farms/router');
const supplyRouter = require('./functions/entity/supplies/router');
const cors = require('cors');

const API_PORT = 3001;
const app = express();
app.use(cors());

// this is our MongoDB database
const dbRoute = "mongodb://scott:qpow1092@ds149742.mlab.com:49742/microorganism";

// connects our back end code with the database
mongoose.connect(
    dbRoute,
    { useNewUrlParser: true }
);

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));

app.use('/api', farmRouter);
app.use('/api', supplyRouter);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));

